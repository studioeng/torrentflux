﻿<?php

/* $Id$ */

/**************************************************************************/
/* TorrentFlux - PHP Torrent Client
/* ============================================
/*
/* This is the language file with all the system messages
/*
/* If you would like to make a translation, please email qrome@yahoo.com
/* the translated file. Please keep the original text order,
/* and just one message per line, also double check your translation!
/*
/* You need to change the second quoted phrase, not the capital one!
/*
/* If you need to use double quotes (") remember to add a backslash (\),
/* so your entry will look like: This is \"double quoted\" text.
/* And, if you use HTML code, please double check it.
/**************************************************************************/

// to be able to exec shell commands with utf8 accents
define("_LC_CTYPE", "fr_FR.UTF-8");
define("_CHARSET", "utf-8");  // utf-8 (check this file remains utf-8 encoded)

define("_SELECTFILE", "Uploader un torrent depuis votre poste");
define("_URLFILE","URL de fichier.torrent depuis un site web externe (https géré !)");
define("_UPLOAD", "Envoyer");
define("_GETFILE", "Ajouter le torrent");
define("_LINKS", "Liens");
define("_ONLINE", "En ligne");
define("_OFFLINE", "Hors ligne");
define("_STORAGE", "Disque dur");
define("_DRIVESPACE", "Espace Disque");
define("_SERVERSTATS", "Stats du serveur / Qui");
define("_DIRECTORYLIST", "Listing répertoire");
define("_ALL", "Tous");
define("_PAGEWILLREFRESH", "La Page se recharge toutes les");
define("_SECONDS", "secondes");
define("_TURNONREFRESH", "Activer le Rechargement Auto");
define("_TURNOFFREFRESH", "Arrêter le Rechargement Auto");
define("_WARNING", "AVERTISSEMENT");
define("_DRIVESPACEUSED", "L'espace disque est saturé!");
define("_ADMINMESSAGE", "Vous avez un message d'un administrateur dans votre boîte de messages.");
define("_TORRENTS", "Torrents");
define("_UPLOADHISTORY", "Historique");
define("_MYPROFILE", "Éditer Mon Profil");
define("_ADMINISTRATION", "Administration");
define("_SENDMESSAGETO", "Envoyez un message à");
define("_TRANSFERFILE", "Fichier Torrent");
define("_FILESIZE", "Taille Du Fichier");
define("_STATUS", "Statut");
define("_ADMIN", "Admin");
define("_BADFILE", "Mauvais Fichier");
define("_DATETIMEFORMAT", "d/m/y H:i");
define("_DATEFORMAT", "d/m/y");
define("_ESTIMATEDTIME", "Temps Estimé");
define("_DOWNLOADSPEED", "Vitesse Download");
define("_UPLOADSPEED", "Vitesse Upload");
define("_SHARING", "Partage");
define("_DONE", "FAIT");
define("_INCOMPLETE", "INACHEVÉ");
define("_NEW", "NOUVEAU");
define("_TRANSFERDETAILS", "Details du Transfert");
define("_STOPTRANSFER", "Stopper le Transfert");
define("_RUNTRANSFER", "Lancer le Transfert");
define("_SEEDTRANSFER", "Partager le Transfert");
define("_DELETE", "Effacer");
define("_ABOUTTODELETE", "Vous êtes sur le point de supprimer");
define("_NOTOWNER", "Pas proprietaire du Transfert");
define("_MESSAGETOALL", "Ce message a ete envoye à TOUS LES UTILISATEURS");
define("_TRYDIFFERENTUSERID", "Erreur: Essayez un utilisateur different.");
define("_HASBEENUSED", "a été utilisé.");
define("_RETURNTOEDIT", "Retour à l'édition");
define("_ADMINUSERACTIVITY", "Administration - Activité D'Utilisateurs");
define("_ADMIN_MENU", "admin");
define("_ACTIVITY_MENU", "activité");
define("_LINKS_MENU", "liens");
define("_NEWUSER_MENU", "nouvel utilisateur");
define("_BACKUP_MENU", "sauvegarde");
define("_ALLUSERS", "Tous les Utilisateurs");
define("_NORECORDSFOUND", "Pas de données");
define("_SHOWPREVIOUS", "Précédent");
define("_SHOWMORE", "Voir Plus");
define("_ACTIVITYSEARCH", "Activité des Recherches");
define("_FILE", "Fichier");
define("_ACTION", "Action");
define("_SEARCH", "Rechercher");
define("_ACTIVITYLOG", "Fichier de journalisation - Dernier");
define("_DAYS", "Jours");
define("_IP", "IP");
define("_TIMESTAMP", "Temps");
define("_USERDETAILS", "Détails de l'Utilisateur");
define("_HITS", "Hits");
define("_UPLOADACTIVITY", "Activité D'Upload");
define("_JOINED", "à rejoint"); // header for the date when the user joined (became a member)
define("_LASTVISIT", "Dernière Visite"); // header for when the user last visited the site
define("_USERSACTIVITY", "Activité"); // used for popup to display Activity next to users name
define("_NORMALUSER", "Utilisateur Normal"); // used to describe a normal user's account type
define("_ADMINISTRATOR", "Administrateur"); // used to describe an administrator's account type
define("_SUPERADMIN", "Super Admin"); // used to describe Super Admin's account type
define("_EDIT", "Éditer");
define("_USERADMIN", "Administration - Utilisateurs"); // title of page for user administration
define("_EDITUSER", "Éditer L'Utilisateur");
define("_UPLOADPARTICIPATION", "Participation au Téléchargement");
define("_UPLOADS", "Uploads"); // Number of uploads a user has contributed
define("_PERCENTPARTICIPATION", "Pourcentage de Participation");
define("_PARTICIPATIONSTATEMENT", "La participation et les téléchargements basés sur les derniers"); // ends with 15 Days
define("_TOTALPAGEVIEWS", "Pages Vues au Total");
define("_THEME", "Thème");
define("_USERTYPE", "Type d'utilisateur");
define("_NEWPASSWORD", "Nouveau Mot de passe");
define("_CONFIRMPASSWORD", "Confirmez le Mot de passe");
define("_HIDEOFFLINEUSERS", "Masquer les utilisateurs Hors-ligne de la page d'acceuil");
define("_UPDATE", "Mise à jour");
define("_USERIDREQUIRED", "L'identification de l'utilisateur est exigée.");
define("_PASSWORDLENGTH", "Le mot de passe doit avoir 6 caractères ou plus.");
define("_PASSWORDNOTMATCH", "Les mots de passe sont différents.");
define("_PLEASECHECKFOLLOWING", "Veuillez vérifier ces informations"); // Displays errors after this statement
define("_NEWUSER", "Nouvel Utilisateur");
define("_PASSWORD", "Mot de passe");
define("_CREATE", "Créer"); // button text to create a new user
define("_ADMINEDITLINKS", "Administration - Modifier Les Liens");
define("_FULLURLLINK", "URL Complete");
define("_BACKTOPARRENT", "Répertoire Parent");  // indicates going back to parent directory
define("_DOWNLOADDETAILS", "Détails Du Téléchargement");
define("_PERCENTDONE", "Pourcentage Terminé");
define("_RETURNTOTRANSFERS", "Revenir aux Transfers"); // Link at the bottom of each page
define("_DATE", "Date");
define("_WROTE", "a écrit");  // Used in a reply to tag what the user had writen
define("_SENDMESSAGETITLE", "Envoyer un message");  // Title of page
define("_TO", "À");
define("_FROM", "De");
define("_YOURMESSAGE", "Votre Message");
define("_SENDTOALLUSERS", "Envoyez à tous les utilisateurs");
define("_FORCEUSERSTOREAD", "Force Utilisateur(s) à lire"); // Admin option in messaging
define("_SEND", "Envoyer");  // Button to send private message
define("_PROFILE", "Profil");
define("_PROFILEUPDATEDFOR", "Profil mis à jour pour");  // Profile updated for 'username'
define("_REPLY", "Répondre");  // popup text for reply button
define("_MESSAGE", "Message");
define("_MESSAGES", "Messages");  // plural (more than one)
define("_RETURNTOMESSAGES", "Retourner aux messages");
define("_COMPOSE", "Ecrire");  // As in 'Compose a message' for button
define("_LANGUAGE", "Langue"); // label
define("_CURRENTDOWNLOAD", "Current Download");
define("_CURRENTUPLOAD", "Current Upload");
define("_SERVERLOAD", "Server Load");
define("_FREESPACE", "Espace dispo.");
define("_UPLOADED", "Uploaded");
define("_QMANAGER_MENU", "queue");
define("_FLUXD_MENU", "fluxd");
define("_SETTINGS_MENU", "settings");
define("_SEARCHSETTINGS_MENU", "search");
define("_ERRORSREPORTED", "Erreurs");
define("_STARTED", "Démarré");
define("_ENDED", "Fini");
define("_QUEUED", "Queued");
define("_DELQUEUE", "Retrait de la file");
define("_FORCESTOP", "Kill Transfer");
define("_STOPPING", "Arret...");
define("_COOKIE_MENU", "cookies");
define('_TOTALXFER','Total Transfer');
define('_MONTHXFER','Month\'s Transfer');
define('_WEEKXFER','Week\'s Transfer');
define('_DAYXFER','Today\'s Transfer');
define('_XFERTHRU','Transfer thru');
define('_REMAINING','Restant');
define('_TOTALSPEED','Total Speed');
define('_SERVERXFERSTATS','Server Transfer Stats');
define('_YOURXFERSTATS','Vos Statistiques');
define('_OTHERSERVERSTATS','Other Server Stats');
define('_TOTAL','Total');
define('_DOWNLOAD','Download');
define('_MONTHSTARTING','Month Starting');
define('_WEEKSTARTING','Week Starting');
define('_DAY','Jour');
define('_XFER','transfer');
define('_XFER_USAGE','Transfer Usage');
define('_QUEUEMANAGER','Queue Manager');
define('_MULTIPLE_UPLOAD','Multiple Upload');
define('_TDDU','Taille Dossier:');
define("_FULLSITENAME", "Site Name");
define('_MOVE_STRING','Move File/Folder to: ');
define('_DIR_MOVE_LINK', 'Move File/Folder');
define('_MOVE_FILE', 'Fichier/Dossier: ');
define('_MOVE_FILE_TITLE', 'Move Data...');
define('_REN_STRING','Renommer File/Folder : ');
define('_DIR_REN_LINK', 'Renommer File/Folder');
define('_REN_FILE', 'File/Folder: ');
define('_REN_DONE', 'Fini!');
define('_REN_ERROR', 'An error accured, please try again!');
define('_REN_ERR_ARG', 'Wrong argument supplied!');
define('_REN_TITLE', 'Renommer');
define('_ID_PORT','Port');
define('_ID_PORTS','Ports');
define('_ID_CONNECTIONS','Connexions');
define('_ID_HOST','Host');
define('_ID_HOSTS','Hosts');
define('_ID_IMAGES','Images');

define("_USER", "Utilisateur");
define("_USERS", "Utilisateurs");

//NG
define("_EMAILADDRESS","Adresse Email");
define("_CHOOSEAUSER","Utilisateur...");
define("_INBOX","Boîte de Réception");

include('lang-common.php');

?>
